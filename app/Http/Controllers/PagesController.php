<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
// use Illuminate\Routing\Controller as BaseController;

class PagesController extends Controller {

  /**
  * Display the view showing the (static) contact us data
  * @param none
  * @return Response
  */

  protected function contact() {
    return view('pages.contactus');
  }

  protected function about() {
    return view('pages.aboutus');
  }
}
