@extends('pages.master')

@section('title')
  {{$user->firstname}} {{$user->lastname}}
@stop

@section('header')
  project1: {{$user->firstname}} {{$user->lastname}}
@stop

@section('data')
  <div>
    {{-- {{print_r($user->getAttributes())}} --}}
    {{-- @foreach($user->toArray() as $key=>$value)
        {{ $key }}: {{$value}}
    @endforeach --}}

    <table class="mx-auto">
      @foreach($user->toArray() as $key=>$value)
        <tr>
          <td>
            {{$key}}
          </td>
          <td>
            {{$value}}
          </td>
        </tr>
      @endforeach
    </table>
  </div>
@stop

@section ('footer')
  <hr>
  <p>Thank you for using project1</p>
@stop
