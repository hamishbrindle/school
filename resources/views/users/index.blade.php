@extends('pages.master')

@section('title')
  Users
@stop

@section('header')
  Users
@stop

@section('data')
  <br>
  <div>
    @foreach($users as $user)
      <span><a href="{{route('showuser', [$user->id])}}">{{$user->firstname}} {{$user->lastname}}</a></span>
    @endforeach

  </div>
@stop

@section ('footer')
  <hr>
  <p>Browse our wares</p>
@stop
