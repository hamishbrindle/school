@extends('pages.master')

@section('title')
  Our Posts
@stop

@section('header')
  {{$companyName}} - {{$jobTitle}}
@stop

@section('data')
  <div style="width:800px; margin:0 auto;">
  {{$jobDescription}}
</div>

@if(isset($contacts) && count($contacts))
    <h2>Contact us:</h2>
    <ul>
        @foreach($contacts as $contact)
            <li>{{$contact}}</li>
        @endforeach
    </ul>
@else
    <h2>NO CONTACT INFORMATION</h2>
@endif

@stop
