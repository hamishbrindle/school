<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->email,
        'linkedinurl' => $faker->unique()->url,
        'streetaddress' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->state,
        'countryid' => $faker->numberBetween(1, 242),
        'postalzipcode' => $faker->postcode,
        'workphone' => $faker->phoneNumber,
        'workphoneextension' => $faker->numberBetween(0, 1000),
        'mobilephone' => $faker->unique()->phoneNumber,
        // 'password' => $faker->unique()->emai,
        'middlename' => $faker->firstName,

        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
